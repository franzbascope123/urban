import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Barber extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'date',
    required: true,
  })
  entry_date: string;

  @property({
    type: 'string',
    required: true,
  })
  cellphone: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Barber>) {
    super(data);
  }
}

export interface BarberRelations {
  // describe navigational properties here
}

export type BarberWithRelations = Barber & BarberRelations;
