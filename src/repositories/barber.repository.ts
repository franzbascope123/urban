import {DefaultCrudRepository} from '@loopback/repository';
import {Barber, BarberRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class BarberRepository extends DefaultCrudRepository<
  Barber,
  typeof Barber.prototype.id,
  BarberRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Barber, dataSource);
  }
}
