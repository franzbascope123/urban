import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Barber} from '../models';
import {BarberRepository} from '../repositories';

export class BarbersController {
  constructor(
    @repository(BarberRepository)
    public barberRepository : BarberRepository,
  ) {}

  @post('/barbers', {
    responses: {
      '200': {
        description: 'Barber model instance',
        content: {'application/json': {schema: getModelSchemaRef(Barber)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Barber, {
            title: 'NewBarber',
            exclude: ['id'],
          }),
        },
      },
    })
    barber: Omit<Barber, 'id'>,
  ): Promise<Barber> {
    return this.barberRepository.create(barber);
  }

  @get('/barbers/count', {
    responses: {
      '200': {
        description: 'Barber model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Barber)) where?: Where<Barber>,
  ): Promise<Count> {
    return this.barberRepository.count(where);
  }

  @get('/barbers', {
    responses: {
      '200': {
        description: 'Array of Barber model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Barber, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Barber)) filter?: Filter<Barber>,
  ): Promise<Barber[]> {
    return this.barberRepository.find(filter);
  }

  @patch('/barbers', {
    responses: {
      '200': {
        description: 'Barber PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Barber, {partial: true}),
        },
      },
    })
    barber: Barber,
    @param.query.object('where', getWhereSchemaFor(Barber)) where?: Where<Barber>,
  ): Promise<Count> {
    return this.barberRepository.updateAll(barber, where);
  }

  @get('/barbers/{id}', {
    responses: {
      '200': {
        description: 'Barber model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Barber, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.object('filter', getFilterSchemaFor(Barber)) filter?: Filter<Barber>
  ): Promise<Barber> {
    return this.barberRepository.findById(id, filter);
  }

  @patch('/barbers/{id}', {
    responses: {
      '204': {
        description: 'Barber PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Barber, {partial: true}),
        },
      },
    })
    barber: Barber,
  ): Promise<void> {
    await this.barberRepository.updateById(id, barber);
  }

  @put('/barbers/{id}', {
    responses: {
      '204': {
        description: 'Barber PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() barber: Barber,
  ): Promise<void> {
    await this.barberRepository.replaceById(id, barber);
  }

  @del('/barbers/{id}', {
    responses: {
      '204': {
        description: 'Barber DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.barberRepository.deleteById(id);
  }
}
